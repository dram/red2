* You don't want to be here.
* You want to be in [[https://gitlab.com/dram/red][Red]]
#+BEGIN_HTML
<style type="text/css">
<!--
table, td {
  border: hidden;
}
ul {
	font-family: serif;
}
li {
	line-height:1.3em;
}
-->
</style>
<table style="border: hidden;"><tr style="border: hidden;">
  <td style="border: hidden;"><img src="RevisitedjED-grayj.png" alt="Red=Jed, Revisited"></td>
  <td style="border: hidden;"><font size="4"><span style="font-family: Courier New;">
    <b>"<span style="color: rgb(255, 0, 0);">RED</span>" is J<span style="color: rgb(255, 0, 0);">ed</span>, <span style="color: rgb(255, 0, 0);">R</span>evisited.</b>
    </span></font>
    <p style="max-width: 540px;">Jed is a Cross-platform, Console / GUI, Lightning-fast, Highly-stable, emacs-like&#160;editor,
    extensible in the syntactically C-ish language S-Lang.
    <p><span style="font-family: Courier New;"><b>
    &gt; The RED Project aims to make it more powerful and<br>
    &gt; versatile, while also lowering the learning curve.
    </b></span>
  </td>
</tr></table>
<div style="margin-left: 8%; margin-right: 20%;">
#+END_HTML

*** Current Release:
| [[red0.x.0-beta.tar.gz][0.x.0-beta.tar.gz]] | A small set of version*-agnostic .sl and .hlp files, notably a new hypertext help system, a full-featured dired, a trivial-to-use grep interface, and 2 new color schemes. Plus installation instructions in a REAME.txt file. |
| [[red0.x.0-beta.zip][0.x.0-beta.zip]]       | The same, but as .zip instead of .tar.gz                                                                                                                                                                                       |

*** RED Project General Goals for Extending Jed

  - Easier to learn to use & extend:
    - hyperlinked and indexed help; integration w/info, web
    - Ctrl-H [& F1 when platform-appropriate] everywhere
    - menus easier to use/comprehend by novices
    - all modes to have a mode menu
    - more common customizations via custom variables
    - eventually, "Customize for Jed" 
  - Better integration w/OS:
    - under Linux desktops, default to user's 'monospace' font
    - font size change on menu & keybound on all GUI versions
    - "single-instance" startup [option], via d-bus [or sockets?]
    - optional clibboard<-->kill-ring 
  - More, and more powerful, tools:
    - improved dired, isearch; included grep, compare-windows
    - easier to share extensions and docs among host and virtual OSs
    - direct [read, at least] access to files in multi-file archives
    - direct access to external files via cURL
    - new read_mini & read_with_completion options
    - in MSW installer, options to install related apps, fonts, etc. 
  - More editing modes, based in part on:
    - generalize indent and comment code from C
    - when that's not appropriate, use f_indent.sl: add indent on 2nd+ Tab
    - enhanced method of narrowing+pushing modes, for multi-mode documents
    - all editing modes to have context help; calls to Zeal, at minimum
    - new options for define_syntax() & variants of dfa_define_highlight()
      - context-based string delims for /reg-exprs/
      - multiple bi-string [i.e. non-EOL] comment styles
      - alternate html syntax code ('>') allowing in-tag highlighting 

These features will all be "built-in", without having to download multiple modules. And all while keeping startup-file compatibility between GUI & console-mode, and among platform versions [with a minimum of #ifdefs].

*** Roadmap:
| 0.18.0 | late Sept 2016 | 0.x.0-beta plus a fix for an external-interface bug under Windows, and several small tweaks & fixes, set up to be copied into/over a the default directories of a 0.99.18 wjed installation. |
| 0.19.0 | early Oct      | the same, but tweaked to be compatible with a 0.99.19 installation, especially from a Debian apt-get.                                                                                        |
| 0.20.0 | mid Oct        | the same, but compatible with a fresh build of the Jed development version.                                                                                                                  |
| 0.??.1 |                | Another round of improvements, including minibuffer help, compare_windows.                                                                                                                   |
| 0.??.2 |                | Yet another round of improvements, including bash_mode, bat_mode, and shell/ashell improvements.                                                                                             |
| 0.20.3 |                | Finally, changes to the C code, plus a compiled Windows version with an updated or new installer.                                                                                            |
	
***** * There are 4 variants of Jed [console-only jed, thin GUI wrappers xjed, wjed, as well as gtkjed]. There are also 3 versions of Jed in use [0.99.18 is the last wjed.exe available pre-compiled and installable; 0.99.19 is the latest available with an "apt-get install", and pre0.99.20-11X is the development version.]

**** 
#+BEGIN_HTML
Sample showing mode-menu for new "DocBrowse" help system, with dired in
background; color&#160;scheme="tangodark":
#+END_HTML
[[file:jed0180-dired-menu-help2.png]]
#+BEGIN_HTML
</div><hr><small>composed in Jed Org-Mode 0.1(+Experimental Features), with
HTML block at top.</small>
#+END_HTML
